$(document).ready(() => {
  const headerText = {
    init: function () {
      this.dom();
      this.action();
    },
    dom: function () {
      this.$obj = $('.btn-toggle-header');
      this.$component = $('.head-text.hidden')
    },
    action: function () {
      let btn = this.$obj;
      let component = this.$component

      btn.on('click', function () {
        let target = $(this).attr('target')
        $('.head-text.block').removeClass('block').addClass('hidden')
        $(target).removeClass('hidden').addClass('block')

        if (target == '#fashion') {
          $('body').addClass('fashion')
        } else {
          $('body').removeClass('fashion')
        }
      })
    }
  }
  headerText.init()
})